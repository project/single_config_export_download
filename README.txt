CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Single Config Export Download allow you to download single export config yml file.

INSTALLATION
------------

Install the Single Config Export Download module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.

REQUIREMENTS
-------------

* This module requires no modules outside of Drupal core.

CONFIGURATION
-------------

* This module requires no configuration.
* Recommend to clear Drupal cache.

MAINTAINERS
-----------

Current maintainers:
 * Shubham Prakash - https://www.drupal.org/u/shubhamprakash
 * Sandeep Jangra - https://www.drupal.org/u/sandeep_jangra
